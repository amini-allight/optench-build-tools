#!/bin/bash
# Debian dislikes x86_64 in the architecture field
if [ "$HOSTTYPE" = "x86_64" ]
then
    PACKAGE_ARCHITECTURE=amd64
else
    PACKAGE_ARCHITECTURE=$HOSTTYPE
fi
PROJECT_NAME=optench
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-debian-$HOSTTYPE
sudo apt install -y git g++ cmake qt6-base-dev
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME.git" target
mkdir -p "$PROJECT_NAME/DEBIAN"
cd target
mkdir -p build
cd build
export C_INCLUDE_PATH=$C_INCLUDE_PATH:/usr/include/$HOSTTYPE-$OSTYPE/qt6:/usr/include/$HOSTTYPE-$OSTYPE/qt6/QtGui
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/include/$HOSTTYPE-$OSTYPE/qt6:/usr/include/$HOSTTYPE-$OSTYPE/qt6/QtGui
cmake -DCMAKE_INSTALL_PREFIX=/usr ..
make -j$(nproc)
make DESTDIR="../../$PROJECT_NAME" install
cd ../..
cat >"$PROJECT_NAME/DEBIAN/control" <<EOL
Package: $PROJECT_NAME
Version: $BUILD_SWARM_PACKAGE_VERSION
Section: science
Priority: optional
Architecture: $PACKAGE_ARCHITECTURE
Maintainer: Amini Allight <amini.allight@protonmail.com>
Homepage: https://amini-allight.org/$PROJECT_NAME
Description: A virtual optical bench.
Depends: libqt6core6 (>= 6.4.0), libqt6gui6 (>= 6.4.0), libqt6widgets6 (>= 6.4.0)
EOL
dpkg-deb --root-owner-group --build $PROJECT_NAME
mv "$PROJECT_NAME.deb" "$OUTPUT_NAME.deb"
echo "$OUTPUT_NAME.deb" > file-name
