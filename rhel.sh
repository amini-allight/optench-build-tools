#!/bin/sh
PROJECT_NAME=optench
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-rhel-$HOSTTYPE
sudo yum install -y rpmdevtools
rm -rf "$HOME/rpmbuild"
rpmdev-setuptree
cat >"$HOME/rpmbuild/SPECS/$PROJECT_NAME.spec" << EOL
Name:           $PROJECT_NAME
Version:        $BUILD_SWARM_PROGRAM_VERSION
Release:        $BUILD_SWARM_RELEASE_NUMBER
Summary:        A virtual optical bench.
License:        GPLv3+
URL:            https://amini-allight.org/$PROJECT_NAME
Source0:        https://gitlab.com/amini-allight/$PROJECT_NAME
BuildArch:      $HOSTTYPE
BuildRequires:  git
BuildRequires:  g++
BuildRequires:  cmake
BuildRequires:  make
BuildRequires:  qt6-qtbase-devel
Requires:       qt6-qtbase

%description
A virtual optical bench.

%prep
rm -rf $PROJECT_NAME
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME"
cd $PROJECT_NAME

%build
cd $PROJECT_NAME
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr ..
make -j\$(nproc)

%install
cd $PROJECT_NAME/build
make DESTDIR=%{buildroot} install

%files
/usr/bin/$PROJECT_NAME
/usr/share/applications/org.$PROJECT_NAME.$PROJECT_NAME.desktop
/usr/share/icons/hicolor/256x256/apps/org.$PROJECT_NAME.$PROJECT_NAME.png
/usr/share/icons/hicolor/256x256/mimetypes/org.$PROJECT_NAME.$PROJECT_NAME-mimetype.png
/usr/share/licenses/$PROJECT_NAME/LICENSE
/usr/share/man/man1/$PROJECT_NAME.1.gz
/usr/share/metainfo/org.$PROJECT_NAME.$PROJECT_NAME.appdata.xml
/usr/share/mime/packages/org.$PROJECT_NAME.$PROJECT_NAME.xml

%changelog
# empty
EOL
rpmbuild -bb "$HOME/rpmbuild/SPECS/$PROJECT_NAME.spec"
mv "$HOME/rpmbuild/RPMS/$HOSTTYPE/$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION.$HOSTTYPE.rpm" "$OUTPUT_NAME.rpm"
echo "$OUTPUT_NAME.rpm" > file-name
