# Optench Build Tools

Build scripts for building [Optench](https://gitlab.com/amini-allight/optench) using [Build Swarm](https://gitlab.com/amini-allight/build-swarm).

## License

GPL 3.0
