#!/bin/sh
PROJECT_NAME=optench
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-windows-$HOSTTYPE
pacman -S --noconfirm git $MINGW_PACKAGE_PREFIX-gcc $MINGW_PACKAGE_PREFIX-cmake make $MINGW_PACKAGE_PREFIX-qt6-base p7zip
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME.git"
cd $PROJECT_NAME
mkdir -p build
cd build
export CMAKE_GENERATOR="Unix Makefiles"
export C_INCLUDE_PATH=$C_INCLUDE_PATH:/mingw64/include/qt6
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/mingw64/include/qt6
export LDFLAGS="$LDFLAGS -mwindows"
cmake ..
make -j$(nproc)
cd ../..
mkdir -p "$OUTPUT_NAME"
cp "$PROJECT_NAME/bin/$PROJECT_NAME.exe" "$OUTPUT_NAME"
windeployqt6 "$OUTPUT_NAME/$PROJECT_NAME.exe"
cp "/mingw64/bin/libb2-1.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libbrotlicommon.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libbrotlidec.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libbz2-1.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libdouble-conversion.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libfreetype-6.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libgcc_s_seh-1.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libglib-2.0-0.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libgraphite2.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libharfbuzz-0.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libiconv-2.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libicudt74.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libicuin74.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libicuuc74.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libintl-8.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libmd4c.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libpcre2-8-0.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libpcre2-16-0.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libpng16-16.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libstdc++-6.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libwinpthread-1.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/libzstd.dll" "$OUTPUT_NAME"
cp "/mingw64/bin/zlib1.dll" "$OUTPUT_NAME"
cp "$PROJECT_NAME/license" "$OUTPUT_NAME"
cp -r "$PROJECT_NAME/sys/windows/licenses" "$OUTPUT_NAME"
7z a "$OUTPUT_NAME.zip" "$OUTPUT_NAME"
echo "$OUTPUT_NAME.zip" > file-name
