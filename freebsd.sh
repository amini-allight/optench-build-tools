#!/usr/local/bin/bash
# no environment variable is available on FreeBSD by default
if [ "$(uname -m)" = "amd64" ]
then
    HOSTTYPE=x86_64
else
    HOSTTYPE=$(uname -m)
fi
PROJECT_NAME=optench
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-freebsd-$HOSTTYPE
sudo pkg install -y git gcc cmake qt6
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME.git"
mkdir -p package
cd $PROJECT_NAME
mkdir -p build
cd build
export C_INCLUDE_PATH=$C_INCLUDE_PATH:/usr/local/include:/usr/local/include/qt6
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/local/include:/usr/local/include/qt6
export LIBRARY_PATH=$LIBRARY_PATH:/usr/local/lib/qt6
cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..
make -j$(nproc)
make DESTDIR="../../package" install
cd ../..
mkdir -p metadata
cat >metadata/+MANIFEST <<EOL
name: "$PROJECT_NAME"
version: "$BUILD_SWARM_PACKAGE_VERSION"
origin: "science/$PROJECT_NAME"
comment: "A virtual optical bench."
desc: "A virtual optical bench."
arch: "$(uname -m)"
maintainer: "amini.allight@protonmail.com"
www: "https://amini-allight.org/$PROJECT_NAME"
prefix: "/usr/local"
deps: {
    "qt6": {
        "version": "6.4.0",
        "origin": "devel/qt6"
    }
}
files: {
EOL
while IFS= read -r line
do
    FILE_HASH=($(sha256sum "$line"))
    echo "    \"${line#package}\": \"$HASH_RESULT\"," >> metadata/+MANIFEST
done <<< $(find package -type f)
echo "}" >> metadata/+MANIFEST
pkg create -r package -m metadata
mv "$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION.pkg" "$OUTPUT_NAME.pkg"
echo "$OUTPUT_NAME.pkg" > file-name
