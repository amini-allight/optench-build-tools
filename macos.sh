#!/bin/sh
PROJECT_NAME=optench
DISPLAY_NAME=Optench
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-macos-$HOSTTYPE
brew install git gcc cmake make qt create-dmg makeicns
git clone "https://gitlab.com/amini-allight/$PROJECT_NAME.git"
cd $PROJECT_NAME
mkdir -p build
cd build
export C_INCLUDE_PATH=$C_INCLUDE_PATH:/usr/local/include
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/local/include
export LDFLAGS="$LDFLAGS -F/usr/local/lib"
cmake ..
make -j$(sysctl -n hw.ncpu)
cd ../..
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/MacOS"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Resources"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Frameworks"
mkdir -p "dmg/$DISPLAY_NAME.app/Contents/Libraries"
# Temporarily put the executable in the normal location to make Qt happy
cp "$PROJECT_NAME/bin/$PROJECT_NAME" "dmg/$DISPLAY_NAME.app/Contents/MacOS/$DISPLAY_NAME"
cp "$PROJECT_NAME/bin/$PROJECT_NAME" "dmg/$DISPLAY_NAME.app/Contents/MacOS/$PROJECT_NAME"
ln -s "/usr/local/lib" "dmg/lib"
macdeployqt6 "dmg/$DISPLAY_NAME.app"
rm "dmg/lib"
cat >"dmg/$DISPLAY_NAME.app/Contents/MacOS/$DISPLAY_NAME" << EOL
#!/bin/sh
cd "\$(dirname \$0)"
export DYLD_FALLBACK_FRAMEWORK_PATH="\$DYLD_FALLBACK_FRAMEWORK_PATH:\$PWD/../Frameworks"
export DYLD_FALLBACK_LIBRARY_PATH="\$DYLD_FALLBACK_LIBRARY_PATH:\$PWD/../Libraries"
./$PROJECT_NAME
EOL
chmod +x "dmg/$DISPLAY_NAME.app/Contents/MacOS/$DISPLAY_NAME"
cp "$PROJECT_NAME/license" "dmg/$DISPLAY_NAME.app/Contents/Resources"
cp -r "$PROJECT_NAME/sys/macos/licenses" "dmg/$DISPLAY_NAME.app/Contents/Resources"
makeicns -in "$PROJECT_NAME/sys/macos/AppIcon.png" -out "dmg/$DISPLAY_NAME.app/Contents/Resources/AppIcon.icns"
cp "$PROJECT_NAME/sys/macos/Info.plist" "dmg/$DISPLAY_NAME.app/Contents"
mv "dmg/$DISPLAY_NAME.app/Contents/Frameworks/"*.dylib "dmg/$DISPLAY_NAME.app/Contents/Libraries"
mv "dmg/$DISPLAY_NAME.app/Contents/PlugIns/"* "dmg/$DISPLAY_NAME.app/Contents/MacOS"
rm -r "dmg/$DISPLAY_NAME.app/Contents/PlugIns"
rm "dmg/$DISPLAY_NAME.app/Contents/Resources/qt.conf"
cp "/usr/local/lib/libb2.1.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libdbus-1.3.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libdouble-conversion.3.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libfreetype.6.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libglib-2.0.0.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libgraphite2.3.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libgthread-2.0.0.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libharfbuzz.0.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/Cellar/icu4c/73.2/lib/libicudata.73.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/Cellar/icu4c/73.2/lib/libicui18n.73.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/Cellar/icu4c/73.2/lib/libicuuc.73.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libmd4c.0.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
cp "/usr/local/lib/libpng16.16.dylib" "dmg/$DISPLAY_NAME.app/Contents/Libraries"
create-dmg \
    --volname "$DISPLAY_NAME" \
    --volicon "dmg/$DISPLAY_NAME.app/Contents/Resources/AppIcon.icns" \
    --background "$PROJECT_NAME/sys/macos/dmg-background.png" \
    --window-pos 200 120 \
    --window-size 800 400 \
    --icon-size 100 \
    --icon "$DISPLAY_NAME.app" 200 190 \
    --hide-extension "$DISPLAY_NAME.app" \
    --app-drop-link 600 185 \
    --skip-jenkins \
    "$OUTPUT_NAME.dmg" \
    "dmg"
echo "$OUTPUT_NAME.dmg" > file-name
