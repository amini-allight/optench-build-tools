#!/bin/sh
PROJECT_NAME=optench
OUTPUT_NAME=$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-arch-$HOSTTYPE
sudo pacman -Sy
mkdir $PROJECT_NAME
cat >"$PROJECT_NAME/PKGBUILD" << EOL
pkgname=$PROJECT_NAME
pkgver=$BUILD_SWARM_PROGRAM_VERSION
pkgrel=$BUILD_SWARM_RELEASE_NUMBER
pkgdesc="A virtual optical bench."
arch=('$HOSTTYPE')
url="https://gitlab.com/amini-allight/$PROJECT_NAME"
license=('GPL3')
makedepends=('git' 'cmake' 'gcc' 'make')
depends=('qt6-base')
provides=("$PROJECT_NAME")
conflicts=("$PROJECT_NAME")
source=("git+https://gitlab.com/amini-allight/$PROJECT_NAME.git")
md5sums=('SKIP')

prepare() {
    cd $PROJECT_NAME
    git pull origin master
}

build() {
    cd $PROJECT_NAME
    rm -rf build
    mkdir -p build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr ..
    make -j\$(nproc)
}

package() {
    cd $PROJECT_NAME
    cd build
    make DESTDIR="\$pkgdir/" install
}
EOL
cd "$PROJECT_NAME"
makepkg -sr --noconfirm
cd ..
mv "$PROJECT_NAME/$PROJECT_NAME-$BUILD_SWARM_PACKAGE_VERSION-$HOSTTYPE.pkg.tar.zst" "$OUTPUT_NAME.pkg.tar.zst"
echo "$OUTPUT_NAME.pkg.tar.zst" > file-name
